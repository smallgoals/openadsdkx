package com.android.vy.csj;

import android.content.Context;
import android.util.Log;

import com.android.OpenAdSDK.R;
import com.bytedance.sdk.openadsdk.LocationProvider;
import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.bytedance.sdk.openadsdk.TTCustomController;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 可以用一个单例来保存TTAdManager实例，在需要初始化sdk的时候调用
 */
public class TTAdManagerHolder {
    private static final String TAG = "TTAdManagerHolder";

    private static boolean sInit;


    public static TTAdManager get() {
        return TTAdSdk.getAdManager();
    }

    public static void init(final Context context) {
        doInit(context);
    }

    //step1:接入网盟广告sdk的初始化操作，详情见接入文档和穿山甲平台说明
    private static void doInit(Context context) {
        if (!sInit) {
            String appid = AdSwitchUtils.getInstance(context).getOnlineValue(AdSwitchUtils.Ads.Csj.getKey(), R.string.csj_id);
            final String[] idAry = (StringUtils.isNullStr(appid)) ? null : appid.split("##");
            if(!AppUtils.hasAgreePrivacyPolicy() || idAry == null || idAry[0].length() < 5){
                return;
            }

            TTAdSdk.init(context, buildConfig(StringUtils.getAdPosID(idAry[0])));
            TTAdSdk.start(new TTAdSdk.Callback() {
                @Override
                public void success() {
                    sInit = true;
                    Log.i(TAG, "success: " + TTAdSdk.isSdkReady());
                    BaseUtils.addMap("TTAdManagerHolder.doInit", "success");
                }
                public void fail(int code, String msg) {
                    sInit = false;
                    Log.i(TAG, "fail:  code = " + code + " msg = " + msg);
                    BaseUtils.addMap("TTAdManagerHolder.doInit", "fail="+CsjSplashAd.csjErr(code));
                }
            });
        }
    }

    private static TTAdConfig buildConfig(String posID) {
        return new TTAdConfig.Builder()
                .appId(posID)
                .useTextureView(false) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
                .allowShowNotify(true) //是否允许sdk展示通知栏提示
                .debug(BaseUtils.getIsDebug()) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .directDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI) //允许直接下载的网络状态集合
                .supportMultiProcess(false)//是否支持多进程
                //.data("[{\"name\":\"personal_ads_type\",\"value\":0}]")//0：关闭 1：开启个性化推荐
                //.needClearTaskReset()
                //SDK初始化配置-穿山甲广告平台  https://www.csjplatform.com/supportcenter/5397
                .customController(new TTCustomController() {
                    final boolean isCan = AdSwitchUtils.Sws.O11.flag;
                    /**
                     * 是否允许SDK主动使用地理位置信息
                     * @return true可以获取，false禁止获取。默认为true
                     */
                    @Override
                    public boolean isCanUseLocation() {
                        return isCan;
                    }
                    public LocationProvider getTTLocation() {
                        return isCan ? super.getTTLocation() : null;
                    }
                    /**
                     * 是否允许SDK主动获取设备上应用安装列表的采集权限
                     * @return true可以使用，false禁止使用。默认为true
                     */
                    public boolean alist() {
                        return isCan;
                    }
                    /**
                     * 是否允许SDK主动使用手机硬件参数，如：imei
                     * @return true可以使用，false禁止使用。默认为true
                     */
                    public boolean isCanUsePhoneState() {
                        return isCan;
                    }
                    public String getDevImei() {
                        return isCan ? super.getDevImei() : ACacheUtils.getMyIMEI();
                    }
                    /**
                     * 是否允许SDK主动使用ACCESS_WIFI_STATE权限
                     * @return true可以使用，false禁止使用。默认为true
                     */
                    public boolean isCanUseWifiState() {
                        return isCan;
                    }
                    public String getMacAddress() {
                        return isCan ? super.getMacAddress() : null;
                    }
                    /**
                     * 是否允许SDK主动使用WRITE_EXTERNAL_STORAGE权限
                     * @return true可以使用，false禁止使用。默认为true
                     */
                    public boolean isCanUseWriteExternal() {
                        return isCan;
                    }
                    public String getDevOaid() {
                        return isCan ? super.getDevOaid() : null;
                    }
                    /**
                     * 是否允许SDK主动获取ANDROID_ID（4600新增）
                     * @return 默认true 允许 ， false 不允许
                     */
                    public boolean isCanUseAndroidId() {
                        return isCan;
                    }
                })
                .build();
    }
}

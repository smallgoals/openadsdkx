package com.android.vy.csj;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.OpenAdSDK.R;
import com.android.vy.AbstractRewardVideo;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdLoadType;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.bytedance.sdk.openadsdk.TTRewardVideoAd;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.qq.e.comm.util.AdError;

import java.util.HashMap;
import java.util.Map;

public class CsjRewardVideo extends AbstractRewardVideo{
    private final static String TAG = CsjRewardVideo.class.getSimpleName();
    private TTRewardVideoAd sRewardVideoAD = null;

    public CsjRewardVideo(Activity act, boolean volumeOn, XmbRewardVideoADListener callback) {
        super(act, volumeOn, callback);
    }

    private boolean mHasShowDownloadActive = false;
    public void initAd() {
        /** 配置Gdt的开屏ID和设置应用版本号*/
        String csjID = AdSwitchUtils.getInstance(Utils.getApp()).getOnlineValue(AdSwitchUtils.Ads.Csj.getKey(), R.string.csj_id);
        String[] idAry = (csjID == null) ? null : csjID.split("##");
        if (idAry != null && idAry.length >= 6 && idAry[5].length() >= 9) {
            String rewardID = StringUtils.getAdPosID(idAry[5]);
            BaseUtils.addMap("CsjRewardVideo.initAd", rewardID);

            //step1:初始化sdk
            TTAdManager ttAdManager = TTAdManagerHolder.get();

            //step2:(可选，强烈建议在合适的时机调用):申请部分权限，如read_phone_state,防止获取不了imei时候，下载类广告没有填充的问题。
            //TTAdManagerHolder.get().requestPermissionIfNecessary(Utils.getApp());

            //step3:创建TTAdNative对象,用于调用广告请求接口
            TTAdNative mTTAdNative = ttAdManager.createAdNative(Utils.getApp());

            //step4:创建广告请求参数AdSlot,具体参数含义参考文档
            AdSlot adSlot = new AdSlot.Builder()
                    //当此次加载的广告用途未知时使用PRELOAD预加载时，请使用：TTAdLoadType.PRELOAD
                    //此次加载广告的用途是实时加载，当用来作为缓存时，请使用：TTAdLoadType.LOAD
                    .setAdLoadType(TTAdLoadType.LOAD)
                    .setCodeId(rewardID)
                    .build();

            //step5:请求广告
            mTTAdNative.loadRewardVideoAd(adSlot, new TTAdNative.RewardVideoAdListener() {
                @Override
                public void onError(int code, String message) {
                    try {
                        LogUtils.eTag(TAG, "Callback --> onError: " + code +"::"+CsjSplashAd.csjErr(code));
                        BaseUtils.addMap("CsjRewardVideo.initAd.onError", "err=" + CsjSplashAd.csjErr(code) + ",jlspErrNums=" + curErrorNums);
                        if (CsjSplashAd.isRetry(code) && (curErrorNums < MAX_ERROR_NUMS)) {
                            curErrorNums++;
                            LogUtils.e("csjjlsp.curErrorNums:" + curErrorNums);

                            //激励广告错误时，进行加载下一个激励
                            initAd();
                        } else {
                            if(res != null){
                                sRewardVideoAD = null;
                                res.initAd();
                            }else {
                                //其它错误，直接抛出回调
                                if (callback != null) callback.onError(new AdError(code, message));
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                //视频广告加载后，视频资源缓存到本地的回调，在此回调后，播放本地视频，流畅不阻塞。
                public void onRewardVideoCached() {
                    if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "Callback --> rewardVideoAd video cached");
                }
                public void onRewardVideoCached(TTRewardVideoAd ad) {
                    if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "Callback --> rewardVideoAd video cached");
                }
                //视频广告的素材加载完毕，比如视频url等，在此回调后，可以播放在线视频，网络不好可能出现加载缓冲，影响体验。
                public void onRewardVideoAdLoad(TTRewardVideoAd ad) {
                    if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "Callback --> onRewardVideoAdLoad");
                    sRewardVideoAD = ad;

                    //激励视频监听：
                    sRewardVideoAD.setRewardAdInteractionListener(new TTRewardVideoAd.RewardAdInteractionListener() {
                        @Override
                        public void onAdShow() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd show");
                            if(callback != null) callback.onADShow();
                        }
                        public void onAdVideoBarClick() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd bar click");
                        }
                        public void onAdClose() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd close");
                            if(callback != null) callback.onADClose();

                            initAd();
                        }
                        //视频播放完成回调
                        public void onVideoComplete() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd complete");
                            if(callback != null) callback.onVideoComplete();
                        }
                        public void onVideoError() {
                            LogUtils.wTag(TAG, "rewardVideoAd error");
                        }
                        //视频播放完成后，奖励验证回调，rewardVerify：是否有效，rewardAmount：奖励梳理，rewardName：奖励名称
                        public void onRewardVerify(boolean rewardVerify, int rewardAmount, String rewardName, int errorCode, String errorMsg) {
                            String logString = "verify:" + rewardVerify + " amount:" + rewardAmount +
                                    " name:" + rewardName + " errorCode:" + errorCode + " errorMsg:" + errorMsg;
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, logString);
                            if(rewardVerify && callback != null){
                                Map<String, Object> map = new HashMap<>();
                                map.put("rewardVerify", rewardVerify);
                                map.put("rewardAmount", rewardAmount);
                                map.put("rewardName", rewardName);
                                map.put("errorCode", errorCode);
                                map.put("errorMsg", errorMsg);
                                callback.onReward(map);
                            }
                        }
                        public void onRewardArrived(boolean rewardVerify, int rewardAmount, Bundle bundle) {
                            String logString = "rewardPlayAgain verify:" + rewardVerify + " amount:" + rewardAmount +
                                    " bundle:" + bundle;
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, logString);
                            if(rewardVerify && callback != null){
                                Map<String, Object> map = new HashMap<>();
                                map.put("rewardVerify", rewardVerify);
                                map.put("rewardAmount", rewardAmount);
                                map.put("bundle", bundle);
                                callback.onReward(map);
                            }
                        }
                        public void onSkippedVideo() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd has onSkippedVideo");
                        }
                    });
                    //再来一个激励视频监听：
                    sRewardVideoAD.setRewardPlayAgainInteractionListener(new TTRewardVideoAd.RewardAdInteractionListener() {
                        @Override
                        public void onAdShow() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd show");
                            if(callback != null) callback.onADShow();
                        }
                        public void onAdVideoBarClick() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd bar click");
                        }
                        public void onAdClose() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd close");
                            if(callback != null) callback.onADClose();
                        }
                        //视频播放完成回调
                        public void onVideoComplete() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd complete");
                            if(callback != null) callback.onVideoComplete();
                        }
                        public void onVideoError() {
                            LogUtils.wTag(TAG, "rewardVideoAd error");
                        }
                        //视频播放完成后，奖励验证回调，rewardVerify：是否有效，rewardAmount：奖励梳理，rewardName：奖励名称
                        public void onRewardVerify(boolean rewardVerify, int rewardAmount, String rewardName, int errorCode, String errorMsg) {
                            String logString = "rewardPlayAgain verify:" + rewardVerify + " amount:" + rewardAmount +
                                    " name:" + rewardName + " errorCode:" + errorCode + " errorMsg:" + errorMsg;
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, logString);
                            if(rewardVerify && callback != null){
                                Map<String, Object> map = new HashMap<>();
                                map.put("rewardVerify", rewardVerify);
                                map.put("rewardAmount", rewardAmount);
                                map.put("rewardName", rewardName);
                                map.put("errorCode", errorCode);
                                map.put("errorMsg", errorMsg);
                                callback.onReward(map);
                            }
                        }
                        public void onRewardArrived(boolean rewardVerify, int rewardAmount, Bundle bundle) {
                            String logString = "rewardPlayAgain verify:" + rewardVerify + " amount:" + rewardAmount +
                                    " bundle:" + bundle;
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, logString);
                            if(rewardVerify && callback != null){
                                Map<String, Object> map = new HashMap<>();
                                map.put("rewardVerify", rewardVerify);
                                map.put("rewardAmount", rewardAmount);
                                map.put("bundle", bundle);
                                callback.onReward(map);
                            }
                        }
                        public void onSkippedVideo() {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "rewardVideoAd has onSkippedVideo");
                        }
                    });
                    //激励视频下载监听：
                    sRewardVideoAD.setDownloadListener(new TTAppDownloadListener() {
                        @Override
                        public void onIdle() {
                            mHasShowDownloadActive = false;
                        }
                        public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
                            if (!mHasShowDownloadActive) {
                                mHasShowDownloadActive = true;
                                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载中，点击下载区域暂停", Toast.LENGTH_LONG);
                            }
                        }
                        public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载暂停，点击下载区域继续", Toast.LENGTH_LONG);
                        }
                        public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
                            LogUtils.wTag(TAG, "下载失败，点击下载区域重新下载", Toast.LENGTH_LONG);
                        }
                        public void onDownloadFinished(long totalBytes, String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载完成，点击下载区域重新下载", Toast.LENGTH_LONG);
                        }
                        public void onInstalled(String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "安装完成，点击下载区域打开", Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        } else {
            // ad is null
            LogUtils.eTag(TAG,"rewardID is error");
        }
    }

    @Override
    public void showAd() {
        if (sRewardVideoAD != null) {
            if (act != null) {
                sRewardVideoAD.showRewardVideoAd(act);
                sRewardVideoAD = null;
            }

            if(callback != null) callback.isPopping = true;
        } else {
            if(res != null){
                res.showAd();
            }else {
                if (callback != null) callback.onNotReady();
            }
        }
    }
}

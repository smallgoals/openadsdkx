package com.android.vy;

import android.app.Activity;

import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.comm.util.AdError;

import java.util.Map;

/**
 * 抽象激励视频广告类<br>
 */
public abstract class AbstractRewardVideo {
    public abstract static class XmbRewardVideoADListener implements RewardVideoADListener {
        public boolean isPopping;

        /**
         * 激励视频触发激励（观看视频大于一定时长或者视频播放完毕）<br>
         */
        public abstract void onReward(Map<String, Object> map);

        /**
         * 无法弹出视频广告，可以在这个回调进行弹插屏
         */
        public abstract void onNotReady();

        /**
         * 可以弹，但是过程中失败
         */
        public abstract void onPoppingError();

        public void onADLoad() {}
        public void onVideoCached() {}
        public void onADShow() {}
        public void onADExpose() {}
        public void onADClick() {}
        public void onVideoComplete() {}
        public void onADClose() {}

        /**
         * 该回调可能会被调用MAX_ERROR_NUMS次！
         *
         * @param adError
         */
        public void onError(AdError adError) {
            if (isPopping) {
                isPopping = false;
                onPoppingError();
            }
        }
    }

    protected final int MAX_ERROR_NUMS = 10; //拉取视频失败再重复拉取的次数
    protected int curErrorNums = 0;//当前失败次数
    protected AbstractRewardVideo res;  //备用激励视频广告

    protected Activity act;
    protected XmbRewardVideoADListener callback;
    protected boolean volumeOn; //默认有声音

    public AbstractRewardVideo(Activity act, boolean volumeOn, XmbRewardVideoADListener callback) {
        this.act = act;
        this.volumeOn = volumeOn;
        this.callback = callback;
    }

    public abstract void initAd();  //初始化并加载广告
    public abstract void showAd();   //广告显示接口

    //添加备用激励视频广告
    public void addResRewardVideo(AbstractRewardVideo res){
        this.res = res;
    }
}

package com.android.vy;

import android.app.Activity;

import com.android.vy.csj.CsjRewardVideo;
import com.android.vy.gdt.GdtRewardVideo;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 激励视频类<br>
 */
public class RewardVideo extends AbstractRewardVideo {
    AbstractRewardVideo mainRewardVideo;
    public RewardVideo(Activity act, boolean volumeOn, XmbRewardVideoADListener callback) {
        super(act, volumeOn, callback);
        SplashUI.preInit(); // 广告预加载

        if(AdSwitchUtils.Ads.Csj.flag){
            mainRewardVideo = new CsjRewardVideo(act, volumeOn, callback);

            if(AdSwitchUtils.Ads.Qq.flag){
                mainRewardVideo.addResRewardVideo(new GdtRewardVideo(act, volumeOn , callback));
            }
        }else {
            mainRewardVideo = new GdtRewardVideo(act, volumeOn, callback);
        }

        initAd();
    }

    @Override
    public void initAd() {
        if(mainRewardVideo != null){
            mainRewardVideo.initAd();

            if(mainRewardVideo.res != null){
                mainRewardVideo.res.initAd();
            }
        }
    }
    public void showAd() {
        if(mainRewardVideo != null){
            mainRewardVideo.showAd();
        }
    }
}

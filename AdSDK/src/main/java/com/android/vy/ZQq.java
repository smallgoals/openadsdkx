package com.android.vy;

import android.app.Activity;
import android.view.ViewGroup;

import com.android.vy.gdt.GdtSplashAd;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyAlertDialog;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.ZzAdUtils.AbVVV;
import com.qq.e.ads.banner2.UnifiedBannerADListener;
import com.qq.e.ads.banner2.UnifiedBannerView;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.ads.interstitial2.UnifiedInterstitialMediaListener;
import com.qq.e.comm.util.AdError;
import com.qq.e.comm.util.GDTLogger;

/**
 * [1240]默认先用新接口加载2.0横幅和2.0插屏，若报5010时尝试用老接口加载；<br>
 * 2021-6-11 上午10:11:59<br>
 */
public class ZQq extends AbVVV {
	public int hfErrNums = 0;
	public int cpErrNums = 0;
	public int MaxNums = 3;

    UnifiedBannerView bAdV2 = null;
	public void addBarBn(final Activity act, final ViewGroup vg, final String appid) throws Exception {
		BaseUtils.addMap("ZQq.addBarBn","initAd");
		if(!AppUtils.hasGdtPermission(act) && res != null){
			res.addBannerAd(act);
			return;
		}

		SplashUI.preInit();
        /*if(bAdV2 != null){
			bAdV2.destroy();
			bAdV2 = null;
		}*/
		final String[] idAry = appid.split("##");
		bAdV2 = new UnifiedBannerView(act, StringUtils.getAdPosID(idAry[1]),
                new AbstractUnifiedBannerADListener() {
            public void onNoAD(AdError err) {
                BaseUtils.addMap("ZQq.addBarBn.onNoAD", "err="+ GdtSplashAd.gdtErr(err)+",hfErrNums="+hfErrNums);
				// 广告是可以重试的，且错误次数小于3；则再次拉取并显示广告。
				if (GdtSplashAd.isRetry(err) && hfErrNums < MaxNums) {
					try {
						hfErrNums++;
						addBarBn(act, vg, appid);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					if (vg != null) vg.removeAllViews();
					if (res != null) res.addBannerAd(act);
				}
            }
			public void onADExposure() {
				hfErrNums = 0;
			}
			public void onADReceive() {
				//添加应用下载类广告的确认弹窗对话框
				GdtSplashAd.addDownloadConfirmListener(bAdV2);
			}
        });
        vg.removeAllViews();
        vg.addView(bAdV2);
        bAdV2.loadAD();
	}

    UnifiedInterstitialAD iAdV2 = null;
	public void showCPAd(final Activity act, final String appid) throws Exception {
		BaseUtils.addMap("ZQq.showCPAd","initAd");
		if(AppUtils.isClosePushAd() && iAdV2 != null){
			iAdV2.close();
			return;
		}

		if(!AppUtils.hasGdtPermission(act) && res != null){
			res.openCpAd(act);
			return;
		}

		SplashUI.preInit();
		if(iAdV2 != null){
            iAdV2.close();
            iAdV2.destroy();
            iAdV2 = null;
        }
		String[] idAry = appid.split("##");
        iAdV2 = new UnifiedInterstitialAD(act, StringUtils.getAdPosID(idAry[2]),
				new AbstractUnifiedInterstitialADListener() {
					public void onADReceive(){
						cpErrNums = 0;
						if(iAdV2 != null) {
							if(StringUtils.containsIgnoreCase(AppUtils.getMetaData(act, "UMENG_CHANNEL"), "_gg")){
								new MyAlertDialog(act).show(new MyTipDialog.DialogMethod() {
									public void sure() {
										//添加应用下载类广告的确认弹窗对话框
										GdtSplashAd.addDownloadConfirmListener(iAdV2);
										iAdV2.show();
									}
								});
							}else{
								//添加应用下载类广告的确认弹窗对话框
								GdtSplashAd.addDownloadConfirmListener(iAdV2);
								iAdV2.show();
							}
						}
						BaseUtils.addMap("ZQq.showCPAd","onADReceive");
					}

					@Override
					public void onVideoCached() {}
					public void onNoAD(AdError err) {
						BaseUtils.addMap("ZQq.showCPAd.onNoAD", "err="+ GdtSplashAd.gdtErr(err)+",cpErrNums="+cpErrNums);
						// 广告是可以重试的，且错误次数小于3；则再次拉取并显示广告。
						if (GdtSplashAd.isRetry(err) && cpErrNums < MaxNums) {
							try {
								cpErrNums++;
								showCPAd(act, appid);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}else{
							if (res != null) res.openCpAd(act);
						}
					}
					public void onADExposure() {
						cpErrNums = 0;
					}
				});
		iAdV2.setMediaListener(new AbstractUnifiedInterstitialMediaListener() {
			@Override
			public void onVideoError(AdError err) {
				BaseUtils.addMap("ZQq.showCPAd.onVideoError", "err="+ GdtSplashAd.gdtErr(err)+",cpErrNums="+cpErrNums);
			}
		});
        iAdV2.loadAD();
	}

	public abstract static class AbstractUnifiedBannerADListener implements UnifiedBannerADListener {
		@Override
		public void onNoAD(AdError adError) {
			GDTLogger.i("ON BannerAD onNoAD");
		}
		public void onADReceive() {
			GDTLogger.i("ON BannerAD onADReceive");
		}
		public void onADExposure() {
			GDTLogger.i("ON BannerAD onADExposure");
		}
		public void onADClosed() {
			GDTLogger.i("ON BannerAD onADClosed");
		}
		public void onADClicked() {
			GDTLogger.i("ON BannerAD onADClicked");
		}
		public void onADLeftApplication() {
			GDTLogger.i("ON BannerAD onADLeftApplication");
		}
		public void onADOpenOverlay() {
			GDTLogger.i("ON BannerAD onADOpenOverlay");
		}
		public void onADCloseOverlay() {
			GDTLogger.i("ON BannerAD onADCloseOverlay");
		}
	}
	public abstract static class AbstractUnifiedInterstitialADListener implements UnifiedInterstitialADListener {
		@Override
		public void onNoAD(AdError adError) {
			GDTLogger.i("ON InterstitialAD onNoAD");
		}
		public void onADOpened() {
			GDTLogger.i("ON InterstitialAD onNoAD");
		}
		public void onADReceive() {
			GDTLogger.i("ON InterstitialAD onADReceive");
		}
		public void onADExposure() {
			GDTLogger.i("ON InterstitialAD onADExposure");
		}
		public void onADClosed() {
			GDTLogger.i("ON InterstitialAD onADClosed");
		}
		public void onADClicked() {
			GDTLogger.i("ON InterstitialAD onADClicked");
		}
		public void onADLeftApplication() {
			GDTLogger.i("ON InterstitialAD onADLeftApplication");
		}
		public void onRenderSuccess() {
			GDTLogger.i("ON InterstitialAD onRenderSuccess");
		}
		public void onRenderFail() {
			GDTLogger.i("ON InterstitialAD onRenderFail");
		}
	}

	public abstract static class AbstractUnifiedInterstitialMediaListener implements UnifiedInterstitialMediaListener {
		@Override
		public void onVideoInit() {
			GDTLogger.i("ON InterstitialAD onVideoInit");
		}
		public void onVideoLoading() {
			GDTLogger.i("ON InterstitialAD onVideoLoading");
		}
		public void onVideoReady(long l) {
			GDTLogger.i("ON InterstitialAD onVideoReady");
		}
		public void onVideoStart() {
			GDTLogger.i("ON InterstitialAD onVideoStart");
		}
		public void onVideoPause() {
			GDTLogger.i("ON InterstitialAD onVideoPause");
		}
		public void onVideoComplete() {
			GDTLogger.i("ON InterstitialAD onVideoComplete");
		}
		public void onVideoError(AdError adError) {
			GDTLogger.i("ON InterstitialAD onVideoError");
		}
		public void onVideoPageOpen() {
			GDTLogger.i("ON InterstitialAD onVideoPageOpen");
		}
		public void onVideoPageClose() {
			GDTLogger.i("ON InterstitialAD onVideoPageClose");
		}
	}

}

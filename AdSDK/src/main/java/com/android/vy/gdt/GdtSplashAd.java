package com.android.vy.gdt;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.OpenAdSDK.R;
import com.android.vy.AbstractSplashAd;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.PropertiesUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.Ads;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.compliance.ApkDownloadComplianceInterface;
import com.qq.e.comm.compliance.DownloadConfirmCallBack;
import com.qq.e.comm.compliance.DownloadConfirmListener;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.managers.setting.GlobalSetting;
import com.qq.e.comm.util.AdError;

/**
 * [1280]Gdt开屏广告展示类<br>
 * 2021-9-26 上午12:11:59<br>
 */
public class GdtSplashAd extends AbstractSplashAd implements SplashADListener {
    private final static String TAG = GdtSplashAd.class.getSimpleName();
    private SplashAD splashAD;
    private long fetchSplashADTime = 0; //记录拉取广告的时间

    private static GdtSplashAd sGdtSplashAd;
    public static GdtSplashAd getInstance(){
        if(sGdtSplashAd == null){
            sGdtSplashAd = new GdtSplashAd();
        }
        return sGdtSplashAd;
    }

    private GdtSplashAd(){}
    public GdtSplashAd(Activity activity, View view, SplashAdCallback splashAdCallback) {
        super(activity, view, splashAdCallback);
    }

    public void preInit(Context ctx){
        try {
            if(ctx == null) ctx = Utils.getApp();
            String qqID = AdSwitchUtils.getInstance(ctx).getOnlineValue(Ads.Qq.getKey(), R.string.qq_id);
            String[] idAry = (StringUtils.isNullStr(qqID)) ? null : qqID.split("##");
            String appid = (idAry == null) ? null : StringUtils.getAdPosID(idAry[0]);
            //未初始化成功，可再次进行加载
            if ( AppUtils.hasAgreePrivacyPolicy() //防止未同意双协议开启广告
                    && (StringUtils.noNullStr(appid) && appid.length() >= 10)
            ) {
                /*
                * 通过这个接口设置是否获取精准定位的信息（位置，设备id等）<br>
                * false表示不允许获取，true表示允许获取，默认允许获取，该接口只在应用第一次设置时生效<br>
                */
                GlobalSetting.setAgreePrivacyStrategy(false);

                // 通过调用此方法初始化 SDK。如果需要在多个进程拉取广告，每个进程都需要初始化 SDK。
                GDTAdSdk.init(ctx.getApplicationContext(), appid);
                BaseUtils.addMap("GdtSplashAd.preInit", "success");
            }
        } catch (Exception e) {
            e.printStackTrace();
            BaseUtils.addMap("GdtSplashAd.preInit", "fail");
        }
    }
    public static class GdtDownloadConfirmListener implements DownloadConfirmListener {
        @Override
        public void onDownloadConfirm(Activity act, int scenes, String infoUrl, final DownloadConfirmCallBack callBack) {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "scenes:" + scenes + " info url:" + infoUrl);
            BaseUtils.addMap("GdtSplashAd.onDownloadConfirm", "scenes:" + scenes + " info url:" + infoUrl);

            String defMsg = "温馨提示##确认前往下载该应用？##确认##取消";
            String gdt_confirm_text = StringUtils.getValue(AdSwitchUtils.getInstance(act).getOnlineValue("gdt_confirm_text"), defMsg);
            String[] ary = gdt_confirm_text.split("##");
            if(ary == null || ary.length < 4 ){
                ary = defMsg.split("##");
            }
            MyTipDialog.popDialog(act, ary[0], ary[1], ary[2], ary[3],
                    new MyTipDialog.DialogMethod() {
                        public void sure() {
                            if(callBack != null){
                                callBack.onConfirm();
                            }
                        }
                        public void cancel(){
                            if(callBack != null){
                                callBack.onCancel();
                            }
                        }
                    });
        }
    };
    //添加应用下载类广告的二次确认弹窗对话框
    public static void addDownloadConfirmListener(ApkDownloadComplianceInterface callback){
        //【已弃用】O2默认关闭，需要打开时添加vivo@@o2_open##(注：debug/open渠道始终有确认弹窗)
        //【更新】Gdt2默认开启，需要关闭时添加vivo@@gdt2_close##(注：debug/open渠道始终有确认弹窗)
        if(BaseUtils.getIsDebug(Utils.getApp())
                /* || AdSwitchUtils.Sws.O2.flag*/
                || AdSwitchUtils.Sws.Gdt2.flag
        ){
            if(callback != null){
                callback.setDownloadConfirmListener(new GdtDownloadConfirmListener());
            }
        }
    }

    public static String gdtErr(AdError err){
        String msg = "err is null";
        if(err != null){
            //msg = err.getErrorCode()+"::"+err.getErrorMsg();
            String code = err.getErrorCode()+"";
            String m = err.getErrorMsg();
            String[] ary = m.split("：");
            if(ary.length <= 1){
                msg = code + "::" + m;
            }else{
                msg = code+"#"+ary[1].trim()+ "::" + PropertiesUtil.getInstance().getValue(R.raw.gdt_error_code, ary[1].trim());
            }
        }
        return msg;
    }
    public static boolean isRetry(AdError err){
        boolean isOK = true;
        if(err != null){
            isOK = isRetryCode(err.getErrorCode());
        }
        return isOK;
    }
    private static boolean isRetryCode(int code){
        boolean isOK = false;
        //错误码 https://developers.adnet.qq.com/doc/android/union/union_debug
        int[] codes = {
                5001, 5002, 5003,//服务端数据错误/视频素材下载错误/视频素材播放错误
                5004,   //未匹配到合适的广告：此情况下禁止多次重试请求广告，否则可能影响系统对您流量的评价从而影响变现效果
                5007, 5008,//资源加载错误/图片加载错误
                5012,//广告数据过期【部分广告（如激励视频）可以预拉取，拉取广告后广告数据会有存在一个过期时间，当开发者调用展示广告的接口但此时当前时间已经超过过期时间时会返回此错误码】
                5022,//模板激励视频渲染失败
                4011,//开屏广告拉取超时，请自查开屏广告的拉取超时时间设置是否过短
                4015,//同一条广告不允许多次展示【重新拉取广告后再进行展示】
                123456789 //结尾标识
        };
        for(int c : codes){
            if(c == code){
                isOK = true;
                break;
            }
        }
        return isOK;
    }

    public void loadMyAd(){
        /** 配置Gdt的开屏ID和设置应用版本号*/
        String qqID = AdSwitchUtils.getInstance(Utils.getApp()).getOnlineValue(Ads.Qq.getKey(), R.string.qq_id);
        String[] idAry = (qqID == null)? null : qqID.split("##");
        if(idAry != null && idAry.length >= 5){
            String kpID = StringUtils.getAdPosID(idAry[4]);
            BaseUtils.addMap("GdtSplashAd.loadMyAd", kpID);

            if(!AppUtils.hasAgreePrivacyPolicy()) {
                gotoMainUI();
            }else if(kpID.length() < 16) {
                gotoNextReserve();
            }else{
                splashContainer = view.findViewById(R.id.splash_container);
                splashHolder = view.findViewById(R.id.splash_holder);
                fetchSplashAD(activity, splashContainer, StringUtils.getAdPosID(idAry[4]),this);
                BaseUtils.addMap("gdtkp.errNums", errNums);
            }
        }else{
            gotoNextReserve();
        }
    }

    /**
     * 拉取开屏广告，开屏广告的构造方法有3种，详细说明请参考开发者文档。<br>
     * @param activity        展示广告的activity<br>
     * @param adContainer     展示广告的大容器<br>
     * 	SkipView的样式可以由开发者自由定制，其尺寸限制请参考activity_splash.xml或者接入文档中的说明。<br>
     * @param posId           广告位ID<br>
     * @param adListener      广告状态监听器<br>
     */
    private void fetchSplashAD(Activity activity, ViewGroup adContainer, String posId, SplashADListener adListener) {
        fetchSplashADTime = System.currentTimeMillis();
        splashAD = new SplashAD(activity, posId, adListener);
        splashAD.fetchAndShowIn(adContainer);
    }

    @Override
    public void onADPresent() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "SplashADPresent");
        splashHolder.setVisibility(View.INVISIBLE); // 广告展示后一定要把预设的开屏图片隐藏起来
    }

    @Override
    public void onADClicked() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "SplashADClicked");
        BaseUtils.addMap("onADClicked", "SplashADClicked");
    }

    /**
     * 倒计时回调，返回广告还将被展示的剩余时间。<br>
     * 通过这个接口，开发者可以自行决定是否显示倒计时提示，或者还剩几秒的时候显示倒计时<br>
     * @param millisUntilFinished 剩余毫秒数<br>
     */
    @Override
    public void onADTick(long millisUntilFinished) {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "SplashADTick " + millisUntilFinished + "ms");
    }

    @Override
    public void onADDismissed() {
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "SplashADDismissed");
        gotoNext();
    }

    @Override
    public void onNoAD(AdError err) {
        preInit(activity); //Gdt预加载

        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "LoadSplashADFail, eCode=" + gdtErr(err));
        BaseUtils.addMap("GdtSplashAd.onNoAD", "err="+gdtErr(err));

        /**
         * 为防止无广告时造成视觉上类似于"闪退"的情况，设定无广告时页面跳转根据需要延迟一定时间，demo
         * 给出的延时逻辑是从拉取广告开始算开屏最少持续多久，仅供参考，开发者可自定义延时逻辑，如果开发者采用demo
         * 中给出的延时逻辑，也建议开发者考虑自定义minSplashTimeWhenNoAD的值
         **/
        long alreadyDelayMills = System.currentTimeMillis() - fetchSplashADTime;//从拉广告开始到onNoAD已经消耗了多少时间
        long shouldDelayMills = alreadyDelayMills > AD_TIME_OUT ? 0 : AD_TIME_OUT
                - alreadyDelayMills;//为防止加载广告失败后立刻跳离开屏可能造成的视觉上类似于"闪退"的情况，根据设置的minSplashTimeWhenNoAD
        // 计算出还需要延时多久
        if(!isRetry(err) || errNums++ >= MaxNums || shouldDelayMills <= 0){
            gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
        }else{
            loadMyAd();
        }
    }

    @Override
    public void onADExposure(){
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "SplashADExposure");
    }

    @Override
    public void onADLoaded(long expireTimestamp) {
        //添加应用下载类广告的确认弹窗对话框
        addDownloadConfirmListener(splashAD);
    }

}
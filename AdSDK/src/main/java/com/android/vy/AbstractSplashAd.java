package com.android.vy;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * 抽象的开屏广告类<br>
 */
public abstract class AbstractSplashAd {
    //广告回调接口：
    public static abstract class SplashAdCallback{
        public abstract void next(); //进入主界面接口
        public void nextReserve(){nextMainUI();} //广告加载失败时，用于加载其它广告的接口
        public abstract void nextMainUI(); //进入主界面接口
    }
    //开屏广告加载超时时间,建议大于3000,这里为了冷启动第一次加载到广告并且展示,示例设置了3000ms
    protected static final int AD_TIME_OUT = 5000;
    protected int errNums = 0;
    protected int MaxNums = 3;

    //构造函数传入参数：
    protected Activity activity;
    protected View view;

    //通过view获取的的：
    protected ViewGroup splashContainer;
    protected ImageView splashHolder;
    protected SplashAdCallback splashAdCallback;

    public abstract void preInit(Context ctx);
    public abstract void loadMyAd();

    protected AbstractSplashAd(){}
    public AbstractSplashAd(Activity activity, View view, SplashAdCallback splashAdCallback) {
        this.activity = activity;
        this.view = view;
        this.splashAdCallback = splashAdCallback;
    }

    protected void gotoNext(){
        if(splashAdCallback != null){
            splashAdCallback.next();
        }
    }
    protected void gotoNextReserve(){
        if(splashAdCallback != null){
            splashAdCallback.nextReserve();
        }
    }
    protected void gotoMainUI(){
        if(splashAdCallback != null){
            splashAdCallback.nextMainUI();
        }
    }
}
